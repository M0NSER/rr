FROM php:8.1-fpm-alpine

RUN apk update && \
    apk add --no-cache --update --virtual buildDeps autoconf tzdata && \
    apk add --no-cache nginx supervisor  && \
    docker-php-ext-install mysqli pdo_mysql && \
    cp /usr/share/zoneinfo/Europe/Warsaw /etc/localtime && \
    echo "Europe/Warsaw" > /etc/timezone && \
    apk del buildDeps && \
    rm -rf /var/cache/apk/*

ADD docker/nginx.conf /etc/nginx/nginx.conf
ADD docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN chown -R nobody.nobody /run && \
  mkdir -p /var/www/html /var/lib/php7/session /var/lib/php7/wsdlcache /var/tmp/nginx && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/tmp/nginx && \
  chown -R nobody.nobody /var/log/nginx && \
  chown -R nobody.nobody /var/lib/php7/session && \
  chown -R nobody.nobody /var/lib/php7/wsdlcache

USER nobody

EXPOSE 8080
WORKDIR /var/www/html

COPY ./ ./

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
